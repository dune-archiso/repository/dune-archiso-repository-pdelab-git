# [`dune-archiso-repository-pdelab-git`](https://gitlab.com/dune-archiso/dune-archiso-repository-pdelab-git) packages for the Arch Linux

[![pipeline status](https://gitlab.com/dune-archiso/dune-archiso-repository-pdelab-git/badges/main/pipeline.svg)](https://gitlab.com/dune-archiso/dune-archiso-repository-pdelab-git/-/commits/main)
[![coverage report](https://gitlab.com/dune-archiso/dune-archiso-repository-pdelab-git/badges/main/coverage.svg)](https://gitlab.com/dune-archiso/dune-archiso-repository-pdelab-git/-/commits/main)

This is a third-party auto-updated repository with the following [tarballs](https://gitlab.com/dune-archiso/dune-archiso-repository-pdelab-git/-/raw/main/packages.x86_64). Mostly packages required for DUNE PDELab module.

## Usage

### Add repository

Add the following code to `/etc/pacman.conf`:

```toml
[dune-archiso-repository-pdelab-git]
SigLevel = Optional TrustAll
Server = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-pdelab-git/$arch
```

### List packages

To show actual packages list:

```console
[user@hostname ~]$ pacman -Sl dune-archiso-repository-pdelab-git
```

### Install packages

To install package:

```console
[user@hostname ~]$ pacman -Syy
[user@hostname ~]$ pacman -S dune-pdelab-git
```

### `PKGBUILD`s from the [Arch User Repository](https://wiki.archlinux.org/title/Arch_User_Repository) 👀

- [`dune-pdelab`](https://aur.archlinux.org/packages/dune-pdelab)